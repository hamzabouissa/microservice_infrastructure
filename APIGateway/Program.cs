using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;

namespace APIGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // CreateHostBuilder(args).Build().Run();
            // BuildWebHost(args).Run();
           
            BuildWebHost(args) 
                .Build()
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            // Host.CreateDefaultBuilder(args).UseServiceProviderFactory(new AutofacServiceProviderFactory())
            // .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webHostBuilder =>
                {
                    webHostBuilder.UseUrls("http://*:8080");
                    webHostBuilder
                        .UseStartup<Startup>();
                });
        
        public static IWebHostBuilder BuildWebHost(string[] args) => new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config
                        .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("appsettings.json", true, true)
                        .AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true,
                            true)
                        .AddJsonFile(Path.Combine("Configuration", "configuration.json"))
                        .AddEnvironmentVariables();
                })
                .ConfigureServices(s => { s.AddOcelot().AddConsul(); })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    //add your logging
                })
                .Configure(app => { app.UseOcelot().Wait(); })
                .UseUrls("http://*:8080");
    }
}