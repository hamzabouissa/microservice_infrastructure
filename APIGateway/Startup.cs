using System;
using APIGateway.Consumers;
using APIGateway.Hubs;
using GreenPipes;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace APIGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
           
        }

        public IConfiguration Configuration { get; }
        

        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("Allow-Origin",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
            services.AddControllers().AddControllersAsServices();
            
            
            services.AddRazorPages().AddRazorPagesOptions(options => { options.RootDirectory = "/Views"; });
            services.AddSignalR();
            
            // add masstransit

            // services.AddMassTransit(x =>
            // {
            //     x.AddConsumer<UmraConsumer>();
            //     x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
            //     {
            //         cfg.Host("localhost");
            //         cfg.ReceiveEndpoint("reply", ep =>
            //         {
            //             ep.PrefetchCount = 16;
            //             ep.UseMessageRetry(x => x.Interval(2, 100));
            //         
            //             ep.ConfigureConsumer<UmraConsumer>(context);
            //         
            //         });
            //     }));
            // });
            //
            // services.AddSingleton<IHostedService, MassTransitApiHostedService>();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors("Allow-Origin");
            app.UseRouting();
            app.UseAuthorization();


            app.UseEndpoints(endpoints => { endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapHub<ChatHub>("/chatHub");
                
            });
        }

        
      
        

    }
}