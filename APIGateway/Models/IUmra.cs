namespace MessageTypes
{
    public interface IUmra
    {
        public string Id { get; set; }
        public string AgentName { get; set; }
        public string Total { get; set; }
    }
}