using System;
using System.Collections.Generic;

namespace MessageTypes
{
    [Serializable()]
    public class Request
    {
        public Dictionary<string,object> data { get; set; }
        public string service_queue { get; set; }
    }
}