using System;
using System.Text.Json;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MessageTypes;


namespace APIGateway.Controllers
{
    [Route("[controller]")]
    public class UmraController: ControllerBase
    {
        private readonly ILogger<UmraController> _logger;
        private readonly ISendEndpointProvider _provider;


        public UmraController(ILogger<UmraController> logger,ISendEndpointProvider provider)
        {
            _logger = logger;
            _provider = provider;
        }
       
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Request request)
        {
            var service_queue = request.service_queue;
            var requestObj = JsonSerializer.Serialize(request.data);
            var endpoint = await _provider.GetSendEndpoint(new Uri($"queue:{service_queue}"));
            await endpoint.Send(new RabbitRequest() {data = requestObj});
            return Accepted("Your request accepted");
        }

        [HttpGet]
        public IActionResult Data()
        {
            return Ok();
        }
    }
}