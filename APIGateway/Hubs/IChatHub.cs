using System.Threading.Tasks;

namespace APIGateway.Hubs
{
    public interface IChatHub
    {
        Task ReceiveMessage(string message);
    }
}