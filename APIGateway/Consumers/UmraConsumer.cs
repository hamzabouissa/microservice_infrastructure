using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using MessageTypes;
using System.Text.Json;
using APIGateway.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace APIGateway.Consumers
{
    public class UmraConsumer : IConsumer<RabbitReply>
    {
        private readonly IHubContext<ChatHub> _hub;

        public UmraConsumer(IHubContext<ChatHub> hub)
        {
            _hub = hub;
        }
        public async Task Consume(ConsumeContext<RabbitReply> context)
        {
            await _hub.Clients.All.SendAsync("ReceiveMessage",context.Message.data);

        }
    }
}