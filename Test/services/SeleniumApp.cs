using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using HtmlAgilityPack;
using MassTransit;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using MessageTypes;
using Dapper;
using Npgsql;
using OpenQA.Selenium.Chrome;
using Test.Data;

namespace Test
{
    public class SeleniumApp
    {
        private  Request _request;
        private readonly ISendEndpointProvider _provider;
        private readonly IDbConnection _connection;
        private  IWebDriver driver;
        public SeleniumApp(ISendEndpointProvider provider,IUmraRepo umraRepo)
        {
            _provider = provider;
            _connection = umraRepo.Connection();
        }

        public async Task Run(Request request)
        {
            _request = request;
            init_db();
            Init();
            Login(driver);
            GetMutamersReports(driver);
            Quit();
            await Reply("Service Bab Umra Done!  :)");
            
        }

        private void init_db()
        {
             string sql = @"
                            CREATE TABLE IF NOT EXISTS umra (
                                AgentId INTEGER NOT NULL,
                                AgentName TEXT NOT NULL,
                                Total INTEGER NOT NULL,
                                GroupId INTEGER NOT NULL
                            );";
             _connection.Execute(sql);
        }

        private void Init()
        {
            // ChromeOptions options = new ChromeOptions();
            FirefoxOptions options = new FirefoxOptions();
            driver = new RemoteWebDriver(new Uri("http://selenium-hub:4444/wd/hub"),options);    
            
        }
        
        private void Login(IWebDriver driver)
        {
            
            driver.Navigate().GoToUrl("http://app2.babalumra.com/security/login.aspx");

            driver.FindElement(By.Id("txtUserName")).SendKeys("التيسير");
            driver.FindElement(By.Id("txtPassword")).SendKeys("123456789");
            driver.FindElement(By.Id("lnkLogin")).Click();   
        }

        private void GetMutamersReports(IWebDriver driver)
        {


            
            
            driver.Navigate().GoToUrl("http://app2.babalumra.com/ReportTools/CustomReporte.aspx?RepID=9");
            
            driver.FindElement(By.Name("ctl00$ContentHolder$rdp_P_DATE_P_FROM$dateInput")).SendKeys(_request.StartDate);
            driver.FindElement(By.Name("ctl00$ContentHolder$rdp_P_DATE_P_TO$dateInput")).SendKeys(_request.FinalDate);
            
            // Select All Attributes 
            
            // var attrs = driver.FindElements(By.XPath("//*[contains(@id,\"ctl00_ContentHolder_ckb\")]"));

            
            driver.FindElement(By.Name("ctl00$ContentHolder$btnGetReport")).Click();
            
            
            var pages = driver.FindElement(
                By.XPath("//div[@class=\"rgWrap rgNumPart\"]")
                ).FindElements(By.TagName("a"));

            int total = pages.Count;
            int current = 1;
            var index = 0;

            while (current<=total)
            {
                var headers = driver.FindElements(By.XPath("//th[@class='rgHeader']/a")).ToList();
                index = headers.FindIndex(item 
                    => item.Text=="Total"
                );
                HtmlParser(driver,current,index);
                driver.FindElement(By.XPath("//input[@title=\"Next Page\"]")).Click();
                current++;
                Thread.Sleep(2000);
                
            }


        }

        private void HtmlParser(IWebDriver driver,int current,int index)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(driver.PageSource);
            var nodes = htmlDoc.DocumentNode.SelectNodes("//tr[contains(@id,\"ctl00_ContentHolder_RadGrid1_ctl00\")]");
            List<Umra> umras = new List<Umra>();
            
            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    
                    var elements = node.ChildNodes;
                    int GroupId = int.Parse(elements[3].SelectSingleNode("a").SelectSingleNode("span").InnerHtml);
                    umras.Add(new Umra()
                    {
                        AgentId = int.Parse(elements[1].InnerHtml),
                        AgentName = elements[2].InnerHtml,
                        Total = int.Parse(elements[index+1].InnerHtml),
                        GroupId = GroupId
                    });
                    
                }
            }
            Console.WriteLine($"\n Page : {current} Fetched...\n ");
            Save(umras);
            
        }

        private async Task Reply(string msg)
        {
            
            var endpoint = await _provider.GetSendEndpoint(new Uri("queue:reply"));
            await endpoint.Send(new RabbitReply{data = msg});
        }

        private void Save(List<Umra> umras)
        {
            string sql = "Insert into umra(AgentId,AgentName,Total,GroupId) values (@AgentId,@AgentName,@Total,@GroupId)";
            
            var affectedRows = _connection.Execute(sql, umras);
            Console.WriteLine(affectedRows);
        }

        public void Quit()
        {
            driver?.Quit();
            _connection?.Close();
        } 
    }
}