#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE TABLE umra (
        Id int,
        AgentName varchar(50) ,
        Total int
    )
EOSQL