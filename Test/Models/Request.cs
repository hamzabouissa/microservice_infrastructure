using System.Collections.Generic;

namespace MessageTypes
{
    public class Request
    {
        public string StartDate { get; set; }
        public string FinalDate { get; set; } 
    }
}