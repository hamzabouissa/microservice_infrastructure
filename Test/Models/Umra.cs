namespace MessageTypes
{
    public class Umra
    {
        public int AgentId { get; set; }
        public string AgentName { get; set; }

        public int Total { get; set; }
        public int GroupId { get; set; }
    }
}