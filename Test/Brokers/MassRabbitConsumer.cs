using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MassTransit;
using MessageTypes;
using System.Text.Json;
using System.Text.Json.Serialization;
using Test.Data;

namespace Test
{
    public class MassRabbitConsumer:IConsumer<RabbitRequest>
    {
        private readonly ISendEndpointProvider _provider;
        private readonly IUmraRepo _umraRepo;

        public MassRabbitConsumer(ISendEndpointProvider provider,IUmraRepo umraRepo)
        {
            _provider = provider;
            _umraRepo = umraRepo;
        }

        public async Task Consume(ConsumeContext<RabbitRequest> context)
        {

            // var requestObj = JsonSerializer.Deserialize<Dictionary<string,object>>(context.Message.data);
            // SeleniumApp app = new SeleniumApp(requestObj["StartDate"].ToString(),_provider,_umraRepo);
            // await app.Run();
        }
    }
}