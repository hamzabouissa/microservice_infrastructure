using System.Data;
using Npgsql;

namespace Test.Data
{
    public class UmraRepo : IUmraRepo
    {
        private readonly IDbConnection _connection;

        public UmraRepo()
        {
            _connection = OpenConnection("Host=db;Username=postgres;Password=postgres;Database=seleniumdb");

        }

        public IDbConnection OpenConnection(string connStr)
        {
            var conn = new NpgsqlConnection(connStr);
            conn.Open();
            return conn;

        }

        public void Close()
        {
            _connection.Close();
        }

        IDbConnection IUmraRepo.Connection() => _connection;



    }
}