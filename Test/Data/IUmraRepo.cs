using System.Data;

namespace Test.Data
{
    public interface IUmraRepo
    {
        IDbConnection OpenConnection(string connStr);
        void Close();
        public IDbConnection Connection();
    }
}