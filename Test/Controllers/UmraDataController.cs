using System;
using System.Data;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MessageTypes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Test.Data;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BabUmraController:ControllerBase
    {
        private readonly SeleniumApp _app;
        private readonly IDbConnection _connection;

        public BabUmraController(IUmraRepo umraRepo,SeleniumApp app)
        {
            _app = app;
            _connection = umraRepo.Connection();
        }
        [HttpGet]
        public IActionResult data()
        {
            var umraData =  _connection.Query<Umra>("SELECT * FROM umra");
            return Ok(umraData);
        }
        
        [HttpPost("Query")]
        public async Task<AcceptedResult> Query([FromBody]Request request)
        {
            Task.Run(()=>_app.Run(request));
            // await _app.Run(request);
            return Accepted("Your request accepted");
        }
    }

    
}