
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Test.Data;

namespace Test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddHealthChecks();
            services.AddControllers();

            services.AddTransient<SeleniumApp>();
            services.AddTransient<IUmraRepo,UmraRepo>();
            
            
            services.AddMassTransit(x =>
            {
                x.AddConsumer<MassRabbitConsumer>();
                x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host("rabbitmq");
                    cfg.ReceiveEndpoint("umra", ep =>
                    {
                        ep.PrefetchCount = 16;
                        // ep.UseMessageRetry(x => x.Interval(2, 100));
                    
                        ep.ConfigureConsumer<MassRabbitConsumer>(context);
                    
                    });
                }));
            });
            
            services.AddSingleton<IHostedService, MassTransitApiHostedService>();
            
            
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/HealthCheck");
            });
            
            
           
        }
    }
}
